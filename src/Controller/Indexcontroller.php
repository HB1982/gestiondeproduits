<?php 

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ProduitRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/')]
class Indexcontroller extends AbstractController
{
    #[Route('/', name: 'index_index',methods:['GET'])]
    public function index(ProduitRepository $ProduitRepository)
    {
        $contex= array('titre' => 'coucou' , 
        'produits'=> $ProduitRepository->findAll(), 
        'showEdit'=> false
    );

        return $this->render('base.html.twig',$contex);
    }
}



?>
