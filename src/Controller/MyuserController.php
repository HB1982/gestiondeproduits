<?php

namespace App\Controller;

use App\Entity\Myuser;
use App\Form\MyuserType;
use App\Repository\MyuserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/myuser')]
class MyuserController extends AbstractController
{
    #[Route('/', name: 'myuser_index', methods: ['GET'])]
    public function index(MyuserRepository $myuserRepository): Response
    {
        return $this->render('myuser/index.html.twig', [
            'myusers' => $myuserRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'myuser_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $myuser = new Myuser();
        $form = $this->createForm(MyuserType::class, $myuser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($myuser);
            $entityManager->flush();

            return $this->redirectToRoute('myuser_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('myuser/new.html.twig', [
            'myuser' => $myuser,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'myuser_show', methods: ['GET'])]
    public function show(Myuser $myuser): Response
    {
        return $this->render('myuser/show.html.twig', [
            'myuser' => $myuser,
        ]);
    }

    #[Route('/{id}/edit', name: 'myuser_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Myuser $myuser, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(MyuserType::class, $myuser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('myuser_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('myuser/edit.html.twig', [
            'myuser' => $myuser,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'myuser_delete', methods: ['POST'])]
    public function delete(Request $request, Myuser $myuser, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$myuser->getId(), $request->request->get('_token'))) {
            $entityManager->remove($myuser);
            $entityManager->flush();
        }

        return $this->redirectToRoute('myuser_index', [], Response::HTTP_SEE_OTHER);
    }
}
