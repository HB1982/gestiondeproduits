<?php

namespace ContainerSCU8Gxt;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder08bdc = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer23007 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicPropertiescaab6 = [
        
    ];

    public function getConnection()
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'getConnection', array(), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'getMetadataFactory', array(), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'getExpressionBuilder', array(), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'beginTransaction', array(), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'getCache', array(), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->getCache();
    }

    public function transactional($func)
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'transactional', array('func' => $func), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'wrapInTransaction', array('func' => $func), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'commit', array(), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->commit();
    }

    public function rollback()
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'rollback', array(), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'getClassMetadata', array('className' => $className), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'createQuery', array('dql' => $dql), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'createNamedQuery', array('name' => $name), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'createQueryBuilder', array(), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'flush', array('entity' => $entity), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'clear', array('entityName' => $entityName), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->clear($entityName);
    }

    public function close()
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'close', array(), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->close();
    }

    public function persist($entity)
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'persist', array('entity' => $entity), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'remove', array('entity' => $entity), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'refresh', array('entity' => $entity), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'detach', array('entity' => $entity), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'merge', array('entity' => $entity), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'getRepository', array('entityName' => $entityName), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'contains', array('entity' => $entity), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'getEventManager', array(), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'getConfiguration', array(), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'isOpen', array(), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'getUnitOfWork', array(), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'getProxyFactory', array(), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'initializeObject', array('obj' => $obj), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'getFilters', array(), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'isFiltersStateClean', array(), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'hasFilters', array(), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return $this->valueHolder08bdc->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer23007 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder08bdc) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder08bdc = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder08bdc->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, '__get', ['name' => $name], $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        if (isset(self::$publicPropertiescaab6[$name])) {
            return $this->valueHolder08bdc->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder08bdc;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder08bdc;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder08bdc;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder08bdc;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, '__isset', array('name' => $name), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder08bdc;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder08bdc;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, '__unset', array('name' => $name), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder08bdc;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder08bdc;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, '__clone', array(), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        $this->valueHolder08bdc = clone $this->valueHolder08bdc;
    }

    public function __sleep()
    {
        $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, '__sleep', array(), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;

        return array('valueHolder08bdc');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer23007 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer23007;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer23007 && ($this->initializer23007->__invoke($valueHolder08bdc, $this, 'initializeProxy', array(), $this->initializer23007) || 1) && $this->valueHolder08bdc = $valueHolder08bdc;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder08bdc;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder08bdc;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
