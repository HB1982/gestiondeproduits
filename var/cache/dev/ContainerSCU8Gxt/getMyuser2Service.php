<?php

namespace ContainerSCU8Gxt;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getMyuser2Service extends App_KernelDevDebugContainer
{
    /**
     * Gets the private '.errored..service_locator.YDJbg1R.App\Entity\Myuser' shared service.
     *
     * @return \App\Entity\Myuser
     */
    public static function do($container, $lazyLoad = true)
    {
        throw new RuntimeException('Cannot autowire service ".service_locator.YDJbg1R": it references class "App\\Entity\\Myuser" but no such service exists.');
    }
}
