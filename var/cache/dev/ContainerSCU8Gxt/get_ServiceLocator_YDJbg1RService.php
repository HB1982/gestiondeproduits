<?php

namespace ContainerSCU8Gxt;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class get_ServiceLocator_YDJbg1RService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private '.service_locator.YDJbg1R' shared service.
     *
     * @return \Symfony\Component\DependencyInjection\ServiceLocator
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['.service_locator.YDJbg1R'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($container->getService, [
            'entityManager' => ['services', 'doctrine.orm.default_entity_manager', 'getDoctrine_Orm_DefaultEntityManagerService', false],
            'myuser' => ['privates', '.errored..service_locator.YDJbg1R.App\\Entity\\Myuser', NULL, 'Cannot autowire service ".service_locator.YDJbg1R": it references class "App\\Entity\\Myuser" but no such service exists.'],
        ], [
            'entityManager' => '?',
            'myuser' => 'App\\Entity\\Myuser',
        ]);
    }
}
